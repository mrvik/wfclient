# Worker Farm Client

Call APIs returning a WorkerFarm token directly. wfclient calls worker farm for you and returns only the final result.
At the moment, sending body to the initial request is not supported and user must set Authorization header, it's not automatically generated.
