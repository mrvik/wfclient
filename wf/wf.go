// Package wf has a client lib
package wf

import (
    "bytes"
    "context"
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "net/http"
)

var (
    ErrNoToken=fmt.Errorf("no continue token on response")
)

// Call Worker Farm. Initialize connection using initialRequest.
// Provided context is used on all subsequent requests but not for initialRequest
// workerFarmBase will be appended with the token got on initialRequest
// Authorization header will be copied from initialRequest.
func CallWF(ctx context.Context, initialRequest *http.Request, workerFarmBase string) (finalBuf *bytes.Buffer, err error) {
    auth:=initialRequest.Header.Get("Authorization")
    initialRes, err:=http.DefaultClient.Do(initialRequest)
    if err!=nil {
        return
    }
    if initialRes.StatusCode>299 {
        err=fmt.Errorf("bad http code %s, expected 2XX@", initialRes.Status)
        return
    }
    var token string
    decoder:=json.NewDecoder(initialRes.Body)
    err=decoder.Decode(&token)
    if err!=nil {
        err=fmt.Errorf("error trying to decode json: %s", err)
        return
    }
    if token=="" {
        err=ErrNoToken
        return
    }
    var (
        currentRequest *http.Request
        currentResponse *http.Response
    )
    loop:
    for finalBuf==nil {
        currentRequest, err=http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/%s", workerFarmBase, token), nil)
        if err!=nil{
            panic(err)
        }
        currentRequest.Header.Set("Authorization", auth)
        currentResponse, err=http.DefaultClient.Do(currentRequest)
        if err!=nil{
            return
        }
        switch {
        case currentResponse.StatusCode==http.StatusAccepted:
            continue
        case currentResponse.StatusCode>299:
            err=fmt.Errorf("server responded with error: %s", readErrorString(currentResponse.Body))
            break loop
        default:
            finalBuf=new(bytes.Buffer)
            _, err=finalBuf.ReadFrom(currentResponse.Body)
            if err!=nil {
                return
            }
        }
    }
    return
}

func readErrorString(reader io.Reader) string {
    bs, err:=ioutil.ReadAll(reader)
    if err!=nil {
        panic(err)
    }
    return string(bs)
}
