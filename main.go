// package main runs wf.CallWF with arguments from cmdline
package main

import (
    "context"
    "flag"
    "fmt"
    "gitlab.com/mrvik/wfclient/wf"
    "io"
    "net/http"
    "os"
    "os/signal"
    "syscall"
)

var (
    method, initialURL, workerBase, outputFile string
    authHeader string
)
func main() {
    flag.StringVar(&method, "method", http.MethodGet, "HTTP method to use")
    flag.StringVar(&initialURL, "url", "", "Initial URL for connection (needed)")
    flag.StringVar(&workerBase, "wfbase", "", "Worker farm base URL (needed)")
    flag.StringVar(&authHeader, "auth", "", "Authorization header content. Leave empty to read from HTTP_AUTH")
    flag.StringVar(&outputFile, "o", "", "Output file to write. If empty, will dump data to stdout")
    flag.Parse()
    if initialURL=="" || workerBase=="" {
        panic("url or wfbase are empty, use -h for help")
    }
    if authHeader=="" {
        authHeader=os.Getenv("HTTP_AUTH")
    }
    if authHeader=="" {
        panic("HTTP_AUTH not authHeader are set. Check help")
    }
    var (
        output io.Writer
        err error
    )
    if outputFile!=""{
        output, err=os.OpenFile(outputFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0o0600)
        if err!=nil{
            panic(err)
        }
    }else{
        output=os.Stdout
    }
    globalContext, cancel:=context.WithCancel(context.Background())
    defer cancel()
    go watchSignals(globalContext, cancel)
    initialRequest, err:=http.NewRequestWithContext(globalContext, method, initialURL, nil)
    if err!=nil {
        panic(err)
    }
    initialRequest.Header.Set("Authorization", authHeader)
    response, err:=wf.CallWF(globalContext, initialRequest, workerBase)
    if err!=nil {
        fmt.Fprintf(os.Stderr, "Error on request: %s\n", err)
        os.Exit(1)
    }
    response.WriteTo(output)
}

func watchSignals(ctx context.Context, stop context.CancelFunc) {
    ch:=make(chan os.Signal)
    signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT)
    select {
    case <-ch:
        stop()
    case <-ctx.Done():
    }
}
